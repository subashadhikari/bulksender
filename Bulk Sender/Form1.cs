﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bulk_Sender
{
    public partial class Form1 : Form
    {
        string smtpProvider;
        int portnumber;
        int sentNumber = 0;
        int totalnumber;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            totalnumber = EmailList.Rows.Count - 1;
            totalNumber.Text = totalnumber.ToString();

            if (providerCB.Text.ToLower() == "Microsoft Online".ToLower())
            {
                smtpProvider = "smtp.office365.com";
                portnumber = 587;
            }
            else if (providerCB.Text.ToLower() == "Gmail".ToLower())
            {
                smtpProvider = "smtp.gmail.com";
                portnumber = 587;
            }
            else if (providerCB.Text.ToLower() == "Yahoo".ToLower())
            {
                smtpProvider = "smtp.mail.yahoo.com";
                portnumber = 587;
            }
            else if (providerCB.Text.ToLower() == "Outlook".ToLower())
            {
                smtpProvider = "smtp-mail.outlook.com";
                portnumber = 587;
            }
            else if (providerCB.Text.ToLower() == "Hotmail".ToLower())
            {
                smtpProvider = "smtp.live.com";
                portnumber = 587;
            }
            else if (providerCB.Text.ToLower() == "Live".ToLower())
            {
                smtpProvider = "smtp.live.com";
                portnumber = 587;
            }
            else
            {
                MessageBox.Show("Your Provider Not Supported");
                return;
            }
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.DoWork += Worker_DoWork;
                worker.ProgressChanged += Worker_ProgressChanged;
                worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(" Sent = " + sentNumber + "\n Error : " + ex.InnerException, "Emails not sent");
            }
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Sent All Emails");
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressLabel.Text = sentNumber++.ToString();
            progressBar.Value = (sentNumber / totalnumber) * 100;
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (DataGridViewRow drow in EmailList.Rows)
            {
                if (drow.Cells[EmailColumn.Text].Value != null)
                {
                    SmtpClient smtpClient = new SmtpClient(smtpProvider, 587);
                    NetworkCredential cred = new NetworkCredential(userName.Text, password.Text);
                    smtpClient.Credentials = cred;
                    smtpClient.EnableSsl = true;
                    MailMessage Msg = new MailMessage()
                    {
                        From = new MailAddress(userName.Text, displayName.Text),
                        Subject = subjectTxb.Text,
                        Body = BodyTxb.Text
                    };
                    Msg.ReplyToList.Add(userName.Text);
                    Msg.To.Add(drow.Cells[EmailColumn.Text].Value.ToString());
                    smtpClient.Send(Msg);
                }
                                    (sender as BackgroundWorker).ReportProgress(sentNumber);

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog fDialog = new OpenFileDialog();
            try
            {
                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    String name = SheetName.Text;
                    String constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                   fDialog.FileName.ToString() +
                                    ";Extended Properties='Excel 12.0 XML;HDR=YES;';";

                    OleDbConnection con = new OleDbConnection(constr);
                    OleDbCommand oconn = new OleDbCommand("Select * From [" + name + "$]", con);
                    con.Open();

                    OleDbDataAdapter sda = new OleDbDataAdapter(oconn);
                    DataTable data = new DataTable();
                    sda.Fill(data);
                    EmailList.DataSource = data;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : "+ex.Message);
            }
        }
    }
}
